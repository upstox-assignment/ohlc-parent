var stompClient = null;

function setConnected(connected) {
    $("#sub").prop("disabled", connected);
    $("#unsub").prop("disabled", !connected);
    $("#symbol").prop("disabled", connected);
    $("#interval").prop("disabled", connected);
    if (connected) {
        $("#ohlcBarTable").show();
    }
    else {
        $("#ohlcBarTable").hide();
    }
    $("#ohlcbars").html("");
}

function subscribe() {
    var socket = new SockJS('/ohlc-server');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/ohlcbars', function (res) {
            showBar(res.body);
        });
        // subscribe
        stompClient.send("/app/subscribe", {}, JSON.stringify({'symbol': $("#symbol").val(), 'interval': $("#interval").val()}));
    });
   
}

function unsubscribe() {
    if (stompClient !== null) {
    	// unsubscribe
        stompClient.send("/app/unsubscribe", {}, JSON.stringify({'symbol': $("#symbol").val(), 'interval': $("#interval").val()}));
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function showBar(message) {
    $("#ohlcbars").append("<tr><td>" + message + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#sub" ).click(function() { subscribe(); });
    $( "#unsub" ).click(function() { unsubscribe(); });
});