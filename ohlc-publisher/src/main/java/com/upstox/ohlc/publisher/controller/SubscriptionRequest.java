package com.upstox.ohlc.publisher.controller;

public class SubscriptionRequest {

	private String symbol;

	private int interval;

	public SubscriptionRequest() {
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	@Override
	public String toString() {
		return "SubscriptionRequest[symbol=" + symbol + ", interval=" + interval + "]";
	}
}
