package com.upstox.ohlc.publisher.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;

import com.upstox.ohlc.common.bo.subscription.SubscriptionKey;
import com.upstox.ohlc.common.subscription.SubscriptionManager;
import com.upstox.ohlc.publisher.subscription.OhlcPublisherSubscriptionHandler;

/**
 * WebSocket handler
 * @author agurav
 *
 */
@Controller
public class SubscriptionController {
	private static final Logger log = LoggerFactory.getLogger(SubscriptionController.class);

	@Autowired
	private SubscriptionManager subscriptionManager;

	@Autowired
	private BeanFactory beanFactory;

	@MessageMapping("/subscribe")
	public void subscribe(SubscriptionRequest req) throws Exception {
		log.info("Received subscription request {}", req);
		validate(req);
		SubscriptionKey key = new SubscriptionKey(req.getSymbol(), req.getInterval());
		OhlcPublisherSubscriptionHandler handler = beanFactory.getBean(OhlcPublisherSubscriptionHandler.class, key);
		subscriptionManager.addSubscription(key, handler);
	}

	@MessageMapping("/unsubscribe")
	public void unsubscribe(SubscriptionRequest req) throws Exception {
		log.info("Received unsubscription request {}", req);
		validate(req);
		SubscriptionKey key = new SubscriptionKey(req.getSymbol(), req.getInterval());
		subscriptionManager.removeSubscription(key);
	}

	public void validate(SubscriptionRequest req) {
		if (StringUtils.isEmpty(req.getSymbol())) {
			throw new IllegalArgumentException("symbol name can not be null");
		}
		if(req.getInterval() <=0 ) {
			throw new IllegalArgumentException("interval can not be less than or equal to 0");
		}
	}
}
