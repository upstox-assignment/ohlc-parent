package com.upstox.ohlc.publisher.subscription;

import javax.jms.Destination;

import org.apache.activemq.command.ActiveMQTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import com.upstox.ohlc.common.api.OhlcBar;
import com.upstox.ohlc.common.bo.subscription.Event;
import com.upstox.ohlc.common.bo.subscription.SubscriptionKey;
import com.upstox.ohlc.common.subscription.SubscriptionHandler;

/**
 * This is a publisher's Implementation of {@link SubscriptionHandler}<br>
 * On subscribe, it sends a subscription event to JMS (for a given
 * {@linkplain SubscriptionKey}) and starts populating STOMP endpoints for
 * websocket clients.<br>
 * On unsubscribe, it sends a unsubscribe event to JMS (for a given
 * {@linkplain SubscriptionKey}) and stops populating STOMP endpoints for
 * websocket clients.<br>
 * 
 * @author agurav
 *
 */
public class OhlcPublisherSubscriptionHandler implements SubscriptionHandler, Runnable {
	private static final Logger log = LoggerFactory.getLogger(OhlcPublisherSubscriptionHandler.class);

	private JmsTemplate jmsTemplate;
	private Destination ohlcTopic;
	private String eventQueueName;
	private SubscriptionKey key;
	private volatile boolean stop = false;
	private SimpMessagingTemplate simpMessagingTemplate;

	public OhlcPublisherSubscriptionHandler(SimpMessagingTemplate simpMessagingTemplate, JmsTemplate jmsTemplate,
			SubscriptionKey key, String ohlcTopicName, String eventQueueName) {
		this.simpMessagingTemplate = simpMessagingTemplate;
		this.jmsTemplate = jmsTemplate;
		this.eventQueueName = eventQueueName;
		this.key = key;
		ohlcTopic = new ActiveMQTopic(ohlcTopicName);
	}

	@Override
	public void subscribe() {
		log.info("Starting subscription for {}", key);
		sendSubscriptionMessage();

		Thread t = new Thread(this);
		t.setDaemon(false);
		t.start();
	}

	private void sendSubscriptionMessage() {
		Event event = new Event();
		event.setEvent("subscribe");
		event.setInterval(key.getInterval());
		event.setSymbol(key.getSymbol());
		jmsTemplate.convertAndSend(eventQueueName, event);
	}

	@Override
	public void unsubscribe() {
		log.info("Stopping subscription for {}", key);
		sendUnsubscribeMessage();
		this.stop = true;
	}

	private void sendUnsubscribeMessage() {
		Event event = new Event();
		event.setEvent("unsubscribe");
		event.setInterval(key.getInterval());
		event.setSymbol(key.getSymbol());
		jmsTemplate.convertAndSend(eventQueueName, event);
	}

	@Override
	public void run() {
		while (!stop) {
			Object receivedObj = jmsTemplate.receiveAndConvert(ohlcTopic);
			if (receivedObj != null && receivedObj instanceof OhlcBar) {
				OhlcBar bar = (OhlcBar) receivedObj;
				if (isApplicable(bar)) {
					log.info("Received {}", bar);
					simpMessagingTemplate.convertAndSend("/topic/ohlcbars", bar);
				} else {
					log.trace("Discarding {} since it is not applicable to key {}", bar, key);
				}
			}
		}
		log.info("Stopped subscription handler for {}", key);
	}

	private boolean isApplicable(OhlcBar bar) {
		if (bar.getSymbol().equals(key.getSymbol()) && bar.getInterval() == key.getInterval()) {
			return true;
		}
		return false;
	}

}
