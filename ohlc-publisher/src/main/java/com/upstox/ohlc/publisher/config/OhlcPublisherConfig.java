package com.upstox.ohlc.publisher.config;

import javax.jms.ConnectionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import com.upstox.ohlc.common.bo.subscription.SubscriptionKey;
import com.upstox.ohlc.common.subscription.SubscriptionManager;
import com.upstox.ohlc.publisher.subscription.OhlcPublisherSubscriptionHandler;

@Configuration
public class OhlcPublisherConfig {
	@Value("${app.jms.destination.ohlc}")
	private String ohlcTopicName;

	@Value("${app.jms.destination.event}")
	private String eventQueueName;

	@Autowired
	private JmsTemplate jmsTemplate;

	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;

	@Bean
	public SubscriptionManager subscriptionManager() {
		return new SubscriptionManager();
	}

	@Bean
	@Scope(value = "prototype")
	public OhlcPublisherSubscriptionHandler ohlcPublisherSubscriptionHandler(SubscriptionKey key) {
		return new OhlcPublisherSubscriptionHandler(simpMessagingTemplate, jmsTemplate, key, ohlcTopicName,
				eventQueueName);
	}
	
	@Bean("JmsListenerContainerFactory")
	public JmsListenerContainerFactory<?> JmsListenerContainerFactory(ConnectionFactory connectionFactory,
			DefaultJmsListenerContainerFactoryConfigurer configurer) {
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		// This provides all boot's default to this factory, including the message
		// converter
		configurer.configure(factory, connectionFactory);
		// You could still override some of Boot's default if necessary.
		return factory;
	}

	@Bean // Serialize message content to json using TextMessage
	public MessageConverter jacksonJmsMessageConverter() {
		MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
		converter.setTargetType(MessageType.TEXT);
		converter.setTypeIdPropertyName("_type");
		return converter;
	}
}
