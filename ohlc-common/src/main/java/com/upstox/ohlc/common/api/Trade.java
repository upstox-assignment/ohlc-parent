package com.upstox.ohlc.common.api;

import java.math.BigDecimal;

/**
 * Represents each trade that is processed in the system
 * @author agurav
 *
 */
public class Trade {
	private String symbol;
	private BigDecimal price;
	private BigDecimal quantity;
	/**
	 * time in millis
	 */
	private long time;

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}
	
	@Override
	public String toString() {
		return "Trade [symbol=" + symbol + ", price=" + price + ", quantity=" + quantity + ", time=" + time + "]";
	}

}
