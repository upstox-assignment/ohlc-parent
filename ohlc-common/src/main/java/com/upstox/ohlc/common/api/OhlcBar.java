package com.upstox.ohlc.common.api;

import java.math.BigDecimal;

/**
 * Representation of the computed bar
 * 
 * @author agurav
 *
 */
public class OhlcBar {
	private String event = "ohlc_notify";
	private String symbol;
	private int interval;
	private int barNum;
	private BigDecimal o;
	private BigDecimal h;
	private BigDecimal l;
	private BigDecimal c;
	private BigDecimal volume;

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public int getBarNum() {
		return barNum;
	}

	public void setBarNum(int barNum) {
		this.barNum = barNum;
	}

	public BigDecimal getO() {
		return o;
	}

	public void setO(BigDecimal o) {
		this.o = o;
	}

	public BigDecimal getH() {
		return h;
	}

	public void setH(BigDecimal h) {
		this.h = h;
	}

	public BigDecimal getL() {
		return l;
	}

	public void setL(BigDecimal l) {
		this.l = l;
	}

	public BigDecimal getC() {
		return c;
	}

	public void setC(BigDecimal c) {
		this.c = c;
	}

	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}
	
	@Override
	public String toString() {
		return "OhlcBar[event=" + event + ", symbol=" + symbol + ", barNum=" + barNum
				+ ", o=" + o + ", h=" + h + ", l=" + l + ", c=" + c + ", volume=" + volume + "]";
	}

}
