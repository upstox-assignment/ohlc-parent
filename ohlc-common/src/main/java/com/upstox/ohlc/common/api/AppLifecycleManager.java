package com.upstox.ohlc.common.api;

/**
 * Manages lifecycle for a given app. Every app should provider its own implementation of the same
 * @author agurav
 *
 */
public interface AppLifecycleManager {
	void init();
	void shutdown();
}
