package com.upstox.ohlc.common.api;

import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides base implementation from which other implementations can extend
 * @author agurav
 *
 */
public abstract class BaseLifecycleManager implements AppLifecycleManager {
	private static final Logger log = LoggerFactory.getLogger(BaseLifecycleManager.class);

	@Override
	public void init() {
		log.info("Setting default timezone to UTC");
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	@Override
	public void shutdown() {

	}

}
