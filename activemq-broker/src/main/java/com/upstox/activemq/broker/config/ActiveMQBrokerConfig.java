package com.upstox.activemq.broker.config;

import org.apache.activemq.broker.BrokerService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;

@EnableJms
@Configuration
public class ActiveMQBrokerConfig {
	@Value("${spring.activemq.broker-url}")
	private String jmsEndpoint;

	@Bean
	public BrokerService broker() throws Exception {
		BrokerService broker = new BrokerService();
		broker.addConnector(jmsEndpoint);
		return broker;
	}
}
