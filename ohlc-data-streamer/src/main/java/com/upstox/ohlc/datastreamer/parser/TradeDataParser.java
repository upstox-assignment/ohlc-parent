package com.upstox.ohlc.datastreamer.parser;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.upstox.ohlc.common.api.Trade;

/**
 * Converts string to {@link Trade} object
 * @author agurav
 *
 */
@Component
public class TradeDataParser {
	private static final Logger log = LoggerFactory.getLogger(TradeDataParser.class);

	private ObjectMapper mapper;

	public TradeDataParser() {
		mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		module.addDeserializer(Trade.class, new TradeDeserializer());
		mapper.registerModule(module);
	}

	public Trade parse(String text) {
		try {
			return mapper.readValue(text, Trade.class);
		} catch (JsonProcessingException e) {
			log.error("Returning null. Failed to parse input {}", text);
			return null;
		}
	}

	@SuppressWarnings("serial")
	private static class TradeDeserializer extends StdDeserializer<Trade> {
		//private static final ZoneId ZONEID_UTC = ZoneId.of("UTC");

		protected TradeDeserializer(Class<?> vc) {
			super(vc);
		}

		protected TradeDeserializer() {
			this(null);
		}

		@Override
		public Trade deserialize(JsonParser p, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {
			Trade t = new Trade();
			ObjectCodec codec = p.getCodec();
			JsonNode node = codec.readTree(p);
			t.setSymbol(node.get("sym").asText());
			t.setPrice(new BigDecimal(node.get("P").asText()));
			t.setQuantity(new BigDecimal(node.get("Q").asText()));
			long timeInMillis = TimeUnit.MILLISECONDS.convert(node.get("TS2").longValue(), TimeUnit.NANOSECONDS);
			t.setTime(timeInMillis);
			//t.setTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(timeInMillis), ZONEID_UTC));
			return t;
		}

	}
}
