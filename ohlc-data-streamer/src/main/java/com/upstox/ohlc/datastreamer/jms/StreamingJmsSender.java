package com.upstox.ohlc.datastreamer.jms;

import javax.jms.Destination;

import org.apache.activemq.command.ActiveMQTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.upstox.ohlc.common.api.Trade;

@Component
public class StreamingJmsSender {
	private static final Logger log = LoggerFactory.getLogger(StreamingJmsSender.class);
	
	private Destination destination ;
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	public StreamingJmsSender(@Value("${app.jms.destination.trades}")String destinationName) {		
		log.debug("JMS Streaming sender configured with destination = {}", destinationName);
		this.destination = new ActiveMQTopic(destinationName);
	}

	public void send(Trade trade) {
		log.trace("Sending to JMS:: {}", trade);
		jmsTemplate.convertAndSend(destination, trade);
	}
}
