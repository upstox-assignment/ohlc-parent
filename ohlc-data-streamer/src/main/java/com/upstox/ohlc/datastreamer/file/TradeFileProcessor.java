package com.upstox.ohlc.datastreamer.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.upstox.ohlc.common.api.Trade;
import com.upstox.ohlc.datastreamer.jms.StreamingJmsSender;
import com.upstox.ohlc.datastreamer.parser.TradeDataParser;

/**
 * Reads trade file line-by-line and send trades to JMS destination
 * @author agurav
 *
 */
@Component
public class TradeFileProcessor {
	private static final Logger log = LoggerFactory.getLogger(TradeFileProcessor.class);
	
	@Autowired
	private TradeDataParser tradeDataParser;

	@Autowired
	private StreamingJmsSender jmsSender;

	public void process(String path) {
		log.info("Processing file {}", path);
		File file = new File(path);
		if (file.exists()) {
			try (BufferedReader br = new BufferedReader(new FileReader(file))) {
				String line;
				while ((line = br.readLine()) != null) {
					log.trace("Parsing:: {}", line);
					Trade trade = tradeDataParser.parse(line);
					jmsSender.send(trade);
					sleepForAWhile();
				}
			} catch (IOException e) {
				log.error("File reading failed", e);
			}
			log.info("Deleting file {}", path);
			file.delete();
		}
	}

	private void sleepForAWhile() {
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
