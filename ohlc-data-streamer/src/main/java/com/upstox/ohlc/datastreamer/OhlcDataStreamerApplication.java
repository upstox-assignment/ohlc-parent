package com.upstox.ohlc.datastreamer;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:app-common.properties")
@SpringBootApplication
public class OhlcDataStreamerApplication {
	@Autowired
	private OhlcDataStreamerLifecycleManager lifecycleManager;

	public static void main(String[] args) {
		SpringApplication.run(OhlcDataStreamerApplication.class, args);
	}

	@PostConstruct
	public void init() throws IOException {
		lifecycleManager.init();
	}
	
	@PreDestroy
	public void shutdown() {
		lifecycleManager.shutdown();
	}
}
