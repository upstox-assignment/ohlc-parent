package com.upstox.ohlc.datastreamer;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.upstox.ohlc.common.api.BaseLifecycleManager;
import com.upstox.ohlc.datastreamer.file.TradeFileProcessor;
import com.upstox.ohlc.datastreamer.file.TradeFileWatcher;

@Component
public class OhlcDataStreamerLifecycleManager extends BaseLifecycleManager {
	private static final Logger log = LoggerFactory.getLogger(OhlcDataStreamerLifecycleManager.class);
	
	@Value("${app.streamer.staging.dir}")
	private String stagingDirectory;
	@Value("${app.streamer.staging.filename}")
	private String fileName;
	private TradeFileWatcher tradeFileWatcher;
	
	@Autowired
	private TradeFileProcessor tradeFileReader;

	@Override
	public void init() {
		super.init();
		log.info("Starting trade file watcher");
		try {
			tradeFileWatcher = new TradeFileWatcher(stagingDirectory, fileName, tradeFileReader);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		tradeFileWatcher.start();
	}
	
	@Override
	public void shutdown() {
		log.info("Shutting down trade file watcher");
		tradeFileWatcher.interrupt();
		super.shutdown();
	}
}
