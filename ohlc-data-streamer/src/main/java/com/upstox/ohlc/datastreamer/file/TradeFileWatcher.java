package com.upstox.ohlc.datastreamer.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Keeps looking for trades file and delegates to {@link TradeFileProcessor} once file is found
 * @author agurav
 *
 */
public class TradeFileWatcher extends Thread {
	private static final Logger log = LoggerFactory.getLogger(TradeFileWatcher.class);

	private String tradeFileName;
	private String tradeFilePath;
	private WatchService watchService;
	private TradeFileProcessor tradeFileProcessor;

	public TradeFileWatcher(String stagingDir, String tradeFileName, TradeFileProcessor tradeFileReader)
			throws IOException {
		super();
		this.tradeFileName = tradeFileName;
		this.tradeFilePath = stagingDir + File.separator + tradeFileName;
		this.tradeFileProcessor = tradeFileReader;
		watchService = FileSystems.getDefault().newWatchService();
		Path path = Paths.get(stagingDir);
		path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY);
	}

	@Override
	public void run() {
		log.info("Watching for {}", tradeFilePath);
		if (Files.exists(Paths.get(tradeFilePath))) {
			log.info("File already present. Sending to file processor");
			tradeFileProcessor.process(tradeFilePath);
		}
		WatchKey key;
		try {
			while ((key = watchService.take()) != null) {
				for (WatchEvent<?> event : key.pollEvents()) {
					String fileName = event.context().toString();
					if (fileName.equals(tradeFileName)) {
						log.info("Found trade file. Sending to file processor");
						tradeFileProcessor.process(tradeFilePath);
					} else {
						log.warn("Unexpected file {} found. Ignoring it", fileName);
					}

				}
				key.reset();
			}
		} catch (InterruptedException e) {
			log.info("Trade file watcher thread was interrupted. Shutting down.");
		}
	}

}
