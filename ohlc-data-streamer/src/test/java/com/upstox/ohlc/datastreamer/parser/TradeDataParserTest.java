package com.upstox.ohlc.datastreamer.parser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import com.upstox.ohlc.common.api.Trade;

public class TradeDataParserTest {
	private TradeDataParser parser = new TradeDataParser();

	@Test
	public void testParse() {
		Trade trade = parser.parse(
				"{\"sym\":\"XZECXXBT\", \"T\":\"Trade\",  \"P\":0.01947, \"Q\":0.1, \"TS\":1538409720.3813, \"side\": \"s\", \"TS2\":1538409725339216503}");
		assertNotNull(trade);
		assertEquals("XZECXXBT", trade.getSymbol());
		assertEquals(new BigDecimal("0.01947"), trade.getPrice());
		assertEquals(new BigDecimal("0.1"), trade.getQuantity());
		//assertEquals(LocalDateTime.of(2018, 10, 1, 16, 2, 5, 339000000), trade.getTime());
		System.out.println(trade);
	}
}
