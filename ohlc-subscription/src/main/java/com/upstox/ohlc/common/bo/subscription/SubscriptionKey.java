package com.upstox.ohlc.common.bo.subscription;

/**
 * Combination of trade symbol and polling interval makes a unique subscription key in the system.
 * @author agurav
 *
 */
public class SubscriptionKey {
	protected String symbol;
	protected int interval;

	public SubscriptionKey(String symbol, int interval) {
		super();
		this.symbol = symbol;
		this.interval = interval;
	}

	@Override
	public String toString() {
		return "Subscription[" + symbol + " at " + interval + " seconds]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + interval;
		result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubscriptionKey other = (SubscriptionKey) obj;
		if (interval != other.interval)
			return false;
		if (symbol == null) {
			if (other.symbol != null)
				return false;
		} else if (!symbol.equals(other.symbol))
			return false;
		return true;
	}

	public String getSymbol() {
		return symbol;
	}

	public int getInterval() {
		return interval;
	}
}
