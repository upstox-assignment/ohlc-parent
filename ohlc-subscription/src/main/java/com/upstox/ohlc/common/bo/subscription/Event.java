package com.upstox.ohlc.common.bo.subscription;

public class Event {
	private String event;
	private String symbol;
	private int interval;

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getSymbol() {
		return symbol;
	}

	@Override
	public String toString() {
		return "Event [event=" + event + ", symbol=" + symbol + ", interval=" + interval + "]";
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}
}
