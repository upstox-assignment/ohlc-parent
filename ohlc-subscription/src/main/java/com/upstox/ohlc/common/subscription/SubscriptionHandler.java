package com.upstox.ohlc.common.subscription;

public interface SubscriptionHandler {
	void subscribe();

	void unsubscribe();
}
