package com.upstox.ohlc.common.subscription;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.upstox.ohlc.common.bo.subscription.SubscriptionKey;

/**
 * Takes care of adding, removing of subscriptions.<br>
 * It also starts/stops associated {@linkplain SubscriptionHandler}s appropriately
 * @author agurav
 *
 */
public class SubscriptionManager {
	private static final Logger log = LoggerFactory.getLogger(SubscriptionManager.class);

	private Map<SubscriptionKey, SubscriptionHandler> handlerMap = new HashMap<>();

	public synchronized void addSubscription(SubscriptionKey key, SubscriptionHandler handler) {
		log.info("Trying to add subscription for {}", key);
		if (handlerMap.containsKey(key)) {
			log.info("Subscription {} already exists. Not adding", key);
		} else {
			try {
				handler.subscribe();
				handlerMap.put(key, handler);
				log.info("Subscribed successfully for {}", key);
			} catch (Exception e) {
				log.error("Failed to subscribe for {}. Reason: {}", key, e);
			}
		}
	}

	public synchronized void removeSubscription(SubscriptionKey key) {
		log.info("Trying to remove subscription for {}", key);
		SubscriptionHandler handler = handlerMap.get(key);
		if (handler == null) {
			log.info("Subscription {} does not exist", key);
		} else {
			try {
				handler.unsubscribe();
				log.info("Unsubscribed successfully for {}", key);
			} catch (Exception e) {
				log.error("Failed to unsubscribe for {}. Reason: {}", key, e);
			} finally {
				handlerMap.remove(key);
			}
		}
	}

	public synchronized void removeAllSubscriptions() {
		if (!handlerMap.isEmpty()) {
			log.info("Removing all subscriptions");
			Iterator<Entry<SubscriptionKey, SubscriptionHandler>> iterator = handlerMap.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<SubscriptionKey, SubscriptionHandler> entry = iterator.next();
				SubscriptionHandler handler = entry.getValue();
				SubscriptionKey key = entry.getKey();
				if (handler != null) {
					try {
						handler.unsubscribe();
						log.info("Unsubscribed successfully for {}", key);
					} catch (Exception e) {
						log.error("Failed to unsubscribe for {}. Reason: {}", key, e);
					} finally {
						iterator.remove();
					}
				}
			}
		}
		log.info("Removed all subscriptions");
	}
}
