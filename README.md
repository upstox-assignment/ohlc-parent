# OHLC Analytic Server
Analytical Server which creates "OHLC" (Open/High/Low/Close) time series basedon the 'Trades' input dataset.  
Find [Source Code](https://gitlab.com/upstox-assignment/ohlc-parent) here.
## Getting Started
These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites
Make sure that you have following tools installed(in specified order) on your machine:

* [Java 8](https://www.oracle.com/technetwork/java/javase/overview/java8-2100321.html)
* [Maven 3.2.1 (or similar)](https://maven.apache.org/)

Additionally, this app requires a staging directory be present at `/var/tmp/tradedata`. This is where the app looks for `trades.json` file.

### How To Build
We are using maven to build the project. Assuming that you have maven already installed on your system, run `mvn clean install` from the project base directory.
You should see something like this which means that the build is successful:

```
$ mvn clean install
Java HotSpot(TM) 64-Bit Server VM warning: ignoring option MaxPermSize=256m; support was removed in 8.0
[INFO] Scanning for projects...
[INFO] ------------------------------------------------------------------------
[INFO] Reactor Build Order:
[INFO]
[INFO] ohlc-common
[INFO] ohlc-data-streamer
[INFO] ohlc-subscription
[INFO] ohlc-processor
[INFO] ohlc-reader
[INFO] activemq-broker
[INFO] ohlc-parent

...

[INFO] ------------------------------------------------------------------------
[INFO] Reactor Summary:
[INFO]
[INFO] ohlc-common ....................................... SUCCESS [  4.300 s]
[INFO] ohlc-data-streamer ................................ SUCCESS [  4.464 s]
[INFO] ohlc-subscription ................................. SUCCESS [  0.191 s]
[INFO] ohlc-processor .................................... SUCCESS [  0.350 s]
[INFO] ohlc-reader ....................................... SUCCESS [  0.878 s]
[INFO] activemq-broker ................................... SUCCESS [  0.307 s]
[INFO] ohlc-parent ....................................... SUCCESS [  0.447 s]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 11.393 s
[INFO] Finished at: 2020-02-20T09:50:49+05:30
[INFO] Final Memory: 47M/237M
[INFO] ------------------------------------------------------------------------
$
```

### How To Run
Now that the build is successful, we can simply run the jar files(located at `target` directory) for following services. Note that all services depend on `activemq-broker` hence it must be started first. Recommended order for startup is:  

* **activemq-broker**
	* Run the command
	
	```
	user@host:~/SomePath/ohlc-parent/activemq-broker$ java -jar target/activemq-broker-0.1.0.jar
	```
	
	* Look for successful startup
	
	```
	2020-02-20 09:58:07.916  INFO 72527 --- [           main] c.u.a.broker.ActiveMQBrokerApplication   : Started ActiveMQBrokerApplication in 1.898 seconds (JVM running for 2.466)
	```
* **ohlc-processor**
	* Run the command
	
	```
	user@host:~/SomePath/ohlc-parent/ohlc-processor$ java -jar target/ohlc-processor-0.1.0.jar
	```
	
	* Look for successful startup
	
	```
	2020-02-20 02:19:25.249  INFO 70604 --- [           main] c.u.o.p.OhlcProcessorApplication         : Started OhlcProcessorApplication in 1.4 seconds (JVM running for 1.944)
	```
* **ohlc-publisher**

 	* Run the command
	
	```
	user@host:~/SomePath/ohlc-parent/ohlc-publisher$ java -jar target/ohlc-publisher-0.1.0.jar
	```
	
	* Look for successful startup
	
	```
	2020-02-20 02:19:32.447  INFO 70605 --- [           main] o.s.m.s.b.SimpleBrokerMessageHandler     : Started.
2020-02-20 02:19:32.487  INFO 70605 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2020-02-20 02:19:32.491  INFO 70605 --- [           main] c.u.o.p.OhlcPublisherApplication         : Started OhlcPublisherApplication in 2.615 seconds (JVM running for 3.185)
	```
* **ohlc-data-streamer**
	* Run the command
	
	```
	user@host:~/SomePath/ohlc-parent/ohlc-data-streamer$ java -jar target/ohlc-data-streamer-0.1.0.jar
	```
	* Look for successful startup
	
	```
2020-02-20 02:19:40.775  INFO 70606 --- [           main] c.u.o.d.OhlcDataStreamerLifecycleManager : Starting trade file watcher
2020-02-20 02:19:40.788  INFO 70606 --- [       Thread-2] c.u.o.d.file.TradeFileWatcher            : Watching for /var/tmp/tradedata/trades.json
2020-02-20 02:19:40.872  INFO 70606 --- [           main] c.u.o.d.OhlcDataStreamerApplication      : Started OhlcDataStreamerApplication in 1.226 seconds (JVM running for 1.784)
	```
	
### How To Use
Steps:  

*  **Access Web UI** - `ohlc-publisher` exposes a Web UI which allows us to subscribe to the symbol and lets us choose the interval as well.
It is accssible on `http://localhost:8080/`. Try accessing the URL from any browser(Chrome preferred)
* **Subscribe to a symbol** - Provider symbol name and appropriate bar interval. 3 seconds interval is preferrable to observe the output clearly. Hit subscribe button.
* **Check logs of publisher and processor** - Once you subscribe, `ohlc-publisher` will receive the request. Look for logs which look like:

```
2020-02-20 02:20:00.035  INFO 70605 --- [nboundChannel-7] c.u.o.p.c.SubscriptionController         : Received subscription request SubscriptionRequest[symbol=XXBTZUSD, interval=15]
2020-02-20 02:20:00.041  INFO 70605 --- [nboundChannel-7] c.u.o.c.s.SubscriptionManager            : Trying to add subscription for Subscription[XXBTZUSD at 15 seconds]
2020-02-20 02:20:00.042  INFO 70605 --- [nboundChannel-7] u.o.p.s.OhlcPublisherSubscriptionHandler : Starting subscription for Subscription[XXBTZUSD at 15 seconds]
2020-02-20 02:20:00.307  INFO 70605 --- [nboundChannel-7] c.u.o.c.s.SubscriptionManager            : Subscribed successfully for Subscription[XXBTZUSD at 15 seconds]
```
The `ohlc-processor` will also receive the subscription. Look for logs which look like:

```
2020-02-20 02:20:00.417  INFO 70604 --- [enerContainer-1] c.u.o.p.jms.SubscriptionEventListener    : Received Event [event=subscribe, symbol=XXBTZUSD, interval=15]
2020-02-20 02:20:00.423 DEBUG 70604 --- [enerContainer-1] p.s.OhlcDataProcessorSubscriptionHandler : Creating bar computer for Subscription[XXBTZUSD at 15 seconds]
2020-02-20 02:20:00.427  INFO 70604 --- [enerContainer-1] c.u.o.c.s.SubscriptionManager            : Trying to add subscription for Subscription[XXBTZUSD at 15 seconds]
2020-02-20 02:20:00.428  INFO 70604 --- [enerContainer-1] p.s.OhlcDataProcessorSubscriptionHandler : Starting bar computer
2020-02-20 02:20:00.429  INFO 70604 --- [enerContainer-1] c.u.o.c.s.SubscriptionManager            : Subscribed successfully for Subscription[XXBTZUSD at 15 seconds]
2020-02-20 02:20:00.429  INFO 70604 --- [       Thread-4] c.u.o.p.computer.OhlcBarComputer         : Bar computer started for symbol[XXBTZUSD] and interval[15 seconds]
```
This confirms that the subscription is registered in the system.

* **Provide trades file** - Now that we have subscribed, it is time to provide input data to the system. Keep the `trades.json` file at `/var/tmp/tradedata` location. Now, `ohlc-data-streamer` should pick up the file in few seconds. Look for logs which look like:

```
2020-02-20 02:20:00.789  INFO 70606 --- [       Thread-2] c.u.o.d.file.TradeFileWatcher            : Found trade file. Sending to file processor
2020-02-20 02:20:00.790  INFO 70606 --- [       Thread-2] c.u.o.d.file.TradeFileProcessor          : Processing file /var/tmp/tradedata/trades.json
```

* **See Live feed on Web UI**
* **Check publisher logs** - `ohlc-publisher` also logs the bar output on console. Look for logs:

```
2020-02-20 10:22:03.916  INFO 72851 --- [       Thread-7] u.o.p.s.OhlcPublisherSubscriptionHandler : Received OhlcBar[event=ohlc_notify, symbol=XXBTZUSD, barNum=1, o=6545.2, h=6559.4, l=6528.6, c=6554.9, volume=116.14882643]
2020-02-20 10:22:06.868  INFO 72851 --- [       Thread-7] u.o.p.s.OhlcPublisherSubscriptionHandler : Received OhlcBar[event=ohlc_notify, symbol=XXBTZUSD, barNum=2, o=6545.2, h=6600, l=6528.6, c=0.0, volume=306.49970033]
2020-02-20 10:22:09.867  INFO 72851 --- [       Thread-7] u.o.p.s.OhlcPublisherSubscriptionHandler : Received OhlcBar[event=ohlc_notify, symbol=XXBTZUSD, barNum=3, o=6545.2, h=6600, l=6528.6, c=0.0, volume=484.07014791]
2020-02-20 10:22:12.868  INFO 72851 --- [       Thread-7] u.o.p.s.OhlcPublisherSubscriptionHandler : Received OhlcBar[event=ohlc_notify, symbol=XXBTZUSD, barNum=4, o=6545.2, h=6600, l=6528.6, c=0.0, volume=711.615382690]
```

Once you are done, you can unsubscribe. You can now subscribe to any combination of symbol and interval.

**IMPORTANT NOTE !!!**  - Please note that, once the data streamer has read and processed all the data, you will not be able to see any output since we are not storing any data anywhere. Hence in order to subscribe, it is assumed that there is a live feed coming in. This is by design.

## High Level Design
Idea here is that the 3 processes(streamer, processor and publisher) talk to each other using JMS. This enables asynchrnous processing and helps reduce latency caused by frequent HTTP calls done in traditional RESTful web service architecure.  
In production environment, a natively installed MQ should be used. For the sake of convenience and demo, we are using ActiveMQ embedded in a java process.  

### Services/Processes
This application consists of 4 different services:  

* **activemq-broker** - An embedded, standalone broker to server JMS functionality. We are using follow JMS destinations:
	* `upstocks.in.tradesTopic` - This is the Topic to which `ohlc-data-streamer` submits input trades. We are using Topic in this scenario because we don't want to persist the messages if there are not active listeners. Also we can have multiple listeners for this topic if we are to scale the system.
	* `upstocks.in.ohlcTopic` - This is the Topic to which `ohlc-processor` submits the bar chart data given that there is a subscription. `ohlc-publisher` reads from this Topic and sends the data across to subscribing client using WebSocket.
	* `upstocks.in.eventQueue` - This is a Queue where `ohlc-publisher` will post subscribe/unsubscribe messages. `ohlc-processor` listens to these messages and stops/starts JMS listeners and manages the subscription in the system.
* **ohlc-data-streamer** - Reads trades from the `trades.json` file and submits to JMS. This can be extended to support other dta streaming mechanisms too
* **ohlc-processor** - Reads the trades from JMS and performs bar calculations on them. Processor activates only when a subscription is received for a given trade symbol and bar interval
* **ohlc-publisher** - Receives client subscriptions via WebSocket, maintains subscription list and submits appropriate subscribe/unsubscribe requests to JMS which in turn are interpreted by `ohlc-processor`

### Libraries
There are 2 libraries present in the application which contain some reusable code:  

* **ohlc-common** - Contains some common business object classes and interface. This library also contains the configuration which is common across services in `app-common.properties` file
* **ohlc-subscription** - Contains the common subscription management code which is used by both `ohlc-processor` and `ohlc-publisher`

### Technologies Used
This app is written using java 8 and spring boot 2 mainly. We are also using WebSockets to feed the bar data to end user's browser.

## Scope For Improvements
Due to time constraints I had to simplify the design.

* I was not able to connect to WebSocket using `wscat`. It may have something to do with the way SpringBoot implements WebSockets.
* At the moment, the UI supports running only 1 subscription at a time. Although the backend can support concurrent subscriptions.  
* Currently, the subscription is supported for a unique combination of `trade symbol + interval`. User-level subscription is not supported for now, but can be supported by enhancing `ohlc-publisher` service.
* I have noticed that the `ohlc-processor` is at times not able to keep up with `ohlc-data-streamer` since we using JMS Topic between them. M	essages are dropped if the listener is not active. This is by design choice and can be done in a better way.
* The current architecture is made with keeping the future scalability in mind. If we want to support more load, we can seggregate the source data using some factor such as country-wise trades, trades from certain industries etc. This can be chosen only after getting a good insight of the business. Once we have a fair bit of an idea, we can write a load balance and/or a message router which will distribute the load across multiple instances of the same service.
* Last but not the least, I have noticed that there are some logical bugs :)