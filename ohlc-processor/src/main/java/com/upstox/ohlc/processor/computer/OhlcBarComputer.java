package com.upstox.ohlc.processor.computer;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;

import javax.jms.Destination;

import org.apache.activemq.command.ActiveMQTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;

import com.upstox.ohlc.common.api.OhlcBar;
import com.upstox.ohlc.common.api.Trade;
import com.upstox.ohlc.common.bo.subscription.SubscriptionKey;

/**
 * This is the core object that performs bar calculation.<br>
 * Distinct objects of this class are created per {@link SubscriptionKey}
 * @author agurav
 *
 */
public class OhlcBarComputer implements Runnable {
	private static final Logger log = LoggerFactory.getLogger(OhlcBarComputer.class);

	private static final BigDecimal ZERO = BigDecimal.valueOf(0.0);

	private JmsTemplate jmsTemplate;
	private Destination tradesTopic;
	private Destination ohlcTopic;

	private volatile boolean stop = false;
	private int barNum = 1;
	private String symbol;
	private int intervalInSeconds;

	private OhlcBar currentBar;
	private Instant barStartTime;

	public OhlcBarComputer(JmsTemplate jmsTemplate, String symbol, int intervalInSeconds, String tradesTopicName, String ohlcTopicName) {
		this.symbol = symbol;
		this.intervalInSeconds = intervalInSeconds;
		this.jmsTemplate = jmsTemplate;
		tradesTopic = new ActiveMQTopic(tradesTopicName);
		ohlcTopic = new ActiveMQTopic(ohlcTopicName);
	}

	@Override
	public void run() {
		log.info("Bar computer started for symbol[{}] and interval[{} seconds]", symbol, intervalInSeconds);
		// wait for first trade to appear
		Trade trade = waitForFirstTrade();
		barStartTime = Instant.now();
		currentBar = newBar(trade);
		while (!stop) {
			Instant now = Instant.now();
			Duration duration = Duration.between(barStartTime, now);
			//FIXME[AAG] this comparison is done with current time, not the trade time
			if (duration.getSeconds() >= intervalInSeconds) {
				// send this bar
				if (trade != null) {
					currentBar.setC(trade.getPrice());
				}
				log.trace("Sending Bar {}", currentBar);
				jmsTemplate.convertAndSend(ohlcTopic, currentBar);
				// start next bar
				startNextBar(now);
			}
			trade = receive();
			if (trade != null) {
				updateCurrentBar(trade);
			}
		}
		log.info("Bar computer stopped for symbol[{}] and interval[{} seconds]", symbol, intervalInSeconds);
	}

	private void startNextBar(Instant newStartTIme) {
		barStartTime = newStartTIme;
		barNum++;
		currentBar.setBarNum(barNum);
		currentBar.setC(ZERO);
	}

	private Trade waitForFirstTrade() {
		log.trace("Waiting for 1st trade to appear for {}", symbol);
		Trade trade = null;
		while (!stop && trade == null) {
			trade = receive();
		}
		return trade;
	}

	private Trade receive() {
		Trade trade = null;
		Object receivedObj = jmsTemplate.receiveAndConvert(tradesTopic);
		if (receivedObj != null && receivedObj instanceof Trade) {
			Trade receivedTrade = (Trade) receivedObj;
			if (receivedTrade.getSymbol().equals(symbol)) {
				trade = receivedTrade;
				log.trace("Received {}", trade);
			} else {
				log.trace("Discarding {} . I am only looking for symbol {}", receivedTrade, symbol);
			}
		}
		return trade;
	}

	public void stop() {
		log.info("Stopping bar computer for symbol[{}] and interval[{} seconds]", symbol, intervalInSeconds);
		this.stop = true;
	}

	private OhlcBar newBar(Trade t) {
		OhlcBar b = new OhlcBar();
		b.setSymbol(symbol);
		b.setInterval(intervalInSeconds);
		b.setBarNum(barNum);
		b.setC(ZERO);
		b.setO(t.getPrice());
		b.setH(t.getPrice());
		b.setL(t.getPrice());
		b.setVolume(t.getQuantity());
		return b;
	}

	private void updateCurrentBar(Trade t) {
		if (currentBar.getH().compareTo(t.getPrice()) < 0) {
			currentBar.setH(t.getPrice());
		}
		if (t.getPrice().compareTo(currentBar.getL()) < 0) {
			currentBar.setL(t.getPrice());
		}
		currentBar.setVolume(currentBar.getVolume().add(t.getQuantity()));
	}
}
