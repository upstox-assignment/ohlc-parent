package com.upstox.ohlc.processor.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.upstox.ohlc.common.bo.subscription.Event;
import com.upstox.ohlc.common.bo.subscription.SubscriptionKey;
import com.upstox.ohlc.common.subscription.SubscriptionManager;
import com.upstox.ohlc.processor.subscription.OhlcDataProcessorSubscriptionHandler;

/**
 * Listens to subscribe/unsubscribe events and delegates to {@link SubscriptionManager}
 * @author agurav
 *
 */
@Component
public class SubscriptionEventListener {
	private static final Logger log = LoggerFactory.getLogger(SubscriptionEventListener.class);

	@Autowired
	private SubscriptionManager subscriptionManager;

	@Autowired
	private BeanFactory beanFactory;

	@JmsListener(destination = "${app.jms.destination.event}", containerFactory = "JmsListenerContainerFactory")
	public void onEvent(Event event) {
		log.info("Received {}", event);
		switch (event.getEvent()) {
		case "subscribe":
			SubscriptionKey key = new SubscriptionKey(event.getSymbol(), event.getInterval());
			OhlcDataProcessorSubscriptionHandler subscriptionHandler = beanFactory
					.getBean(OhlcDataProcessorSubscriptionHandler.class, key);
			subscriptionManager.addSubscription(key, subscriptionHandler);
			break;
		case "unsubscribe":
			subscriptionManager.removeSubscription(new SubscriptionKey(event.getSymbol(), event.getInterval()));
			break;
		default:
			log.error("Unknown subscription event {}", event.getEvent());
			break;
		}
	}
}
