package com.upstox.ohlc.processor.subscription;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;

import com.upstox.ohlc.common.bo.subscription.SubscriptionKey;
import com.upstox.ohlc.common.subscription.SubscriptionHandler;
import com.upstox.ohlc.processor.computer.OhlcBarComputer;

public class OhlcDataProcessorSubscriptionHandler implements SubscriptionHandler {
	private static final Logger log = LoggerFactory.getLogger(OhlcDataProcessorSubscriptionHandler.class);
	
	private OhlcBarComputer ohlcBarComputer;

	public OhlcDataProcessorSubscriptionHandler(BeanFactory beanFactory, SubscriptionKey key) {
		log.debug("Creating bar computer for {}", key);
		this.ohlcBarComputer = beanFactory.getBean(OhlcBarComputer.class, key.getSymbol(), key.getInterval());
	}

	@Override
	public void subscribe() {
		log.info("Starting bar computer");
		Thread t = new Thread(ohlcBarComputer);
		t.setDaemon(false);
		t.start();
	}

	@Override
	public void unsubscribe() {
		log.info("Stopping bar computer");
		this.ohlcBarComputer.stop();
	}

}
