package com.upstox.ohlc.processor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.upstox.ohlc.common.api.BaseLifecycleManager;
import com.upstox.ohlc.common.subscription.SubscriptionManager;

@Component
public class OhlcProcessorLifecycleManager extends BaseLifecycleManager {
	@Autowired
	private SubscriptionManager subscriptionManager;

	@Override
	public void init() {
		super.init();
	}

	@Override
	public void shutdown() {
		subscriptionManager.removeAllSubscriptions();
		super.shutdown();
	}

}
