package com.upstox.ohlc.processor;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:app-common.properties")
@SpringBootApplication
public class OhlcProcessorApplication {
	@Autowired
	private OhlcProcessorLifecycleManager lifecycleManager;

	public static void main(String[] args) {
		SpringApplication.run(OhlcProcessorApplication.class, args);
	}

	@PostConstruct
	public void init() {
		lifecycleManager.init();
	}
	
	@PreDestroy
	public void shutdown() {
		lifecycleManager.shutdown();
	}
}
